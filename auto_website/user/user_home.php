<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Vartotojo paskyra</title>
	
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="../dist/css/main.min.css">
</head>
<body>
	
	<div class="container-fluid page">
		<!-- Išspausdinti vartotojo vardą -->
		<!-- Rodyti lentelę automobilių -->

		<div class="container">
		<h1>Automobilių sąrašas:</h1>

		<table class="table table-hover">
			<tr>
				<th>#</th>
				<th>Markė</th>
				<th>Modelis</th>
				<th>Valst. nr.</th>
			</tr>
			<?php
				
				$counter = 1;
				
				foreach ($result as $row) :?>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				<?php
				endforeach;
			?>
		</table>
		</div>
	</div>

	
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	<script src="dist/js/app.min.js"></script>
</body>
</html>